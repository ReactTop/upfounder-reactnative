import { AppButton, SafeView, Header, EditInput, ProfileAvatar } from '@components/index'
import { DefaultStyles, Images, Screens, Color, Dimensions } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import { ActionCreators } from '@redux/action.js'
import React, { Component } from 'react'
import { ImageBackground, Image, Text, View, AsyncStorage, TouchableOpacity, Platform } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from './styles'
import { Icon, Content } from 'native-base'
import ModalDropdown from 'react-native-modal-dropdown';

import ImagePicker from 'react-native-image-picker';
import { Dropdown } from 'react-native-material-dropdown'
import { fileEndpoint } from '@api/Endpoint'
import axios from 'axios'
import { handleCheckPermissions } from '@utils/permission'

const options = {
    title: 'Upload a clear picture',
    maxWidth: 200,
    maxHeight: 200,
    storageOptions: {
        skipBackup: true,
        waitUntilSaved: true,
        cameraRoll: true,
        path: 'images'
    }
}

const createFormData = (photo, body) => {
    const data = new FormData();
    data.append("contactUrl", {
        name: photo.fileName,
        size: photo.fileSize,
        mimetype: photo.type,
        type: photo.type,
        uri: Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", ""),
        path: photo.path || Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", ""),
    });

    Object.keys(body).forEach(key => {
        data.append(key, body[key]);
    });

    return data;
};

const SubDot = <Icon type={'FontAwesome5'} name={'dot-circle'} style={{ fontSize: Dimensions.px14, marginBottom: Dimensions.px10 }} />

const categories = [
    { label: 'Mentors', value: 1 },
    { label: 'Investors', value: 2, },
    { label: 'Other Founders', value: 3 },
    { label: 'Developers', value: 4 },
]

class ContactPage extends Component {
    state = {
        keyStrength: '',
        note: ''
    }
    componentDidMount() {
        handleCheckPermissions()
        this.sub = this.props.navigation.addListener('didFocus', this.handleInit)
    }
    componentWillUnmount() {
        if (this.sub) this.sub.remove()
    }
    handleInit = () => {
        const linkedinUrl = this.props.navigation.getParam('linkedinUrl', null)
        if (linkedinUrl) {
            const user = this.props.Contact.user
            this.props.updateContactDetail('user', { ...user, linkedinUrl })
        }
    }

    changeAvatar() {
        const user = this.props.Contact.user
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                this.props.updateContactDetail('user', { ...user, contactUrl: response })
            }
        });

    }
    verifyEditContact() {
        const { phone, email, name, linkedinUrl, contactUrl, keyStrength, note, category, _id } = this.props.Contact.user
        var oBj2Send = {
            keyStrength,
            note,
            name,
            email,
            phone,
            linkedinUrl,
            category,
        }
        axios
            .post(`${fileEndpoint}/contact/edit/${_id}`, oBj2Send)
            .then(res => {
                if (contactUrl && contactUrl.uri) {
                    axios
                        .post(`${fileEndpoint}/contact/editPhoto/${_id}`, createFormData(contactUrl, {}), {
                            headers: {
                                'content-type': 'multipart/form-data'
                            }
                        })
                        .then(res => {
                            console.log('Success/editPhoto', res.body)
                            NavigationService.goBack()
                        })
                        .catch(err => {
                            console.log('ERROR=>>editPhoto', err)
                        })
                } else {
                    NavigationService.goBack()
                }

            })
            .catch(err => {
                console.log('ERROR=>>edit', err)
            })


    }
    render() {
        const { updateContactDetail } = this.props
        const { user } = this.props.Contact
        const { phone, email, name, linkedinUrl, photoUrl, contactUrl, category, keyStrength, note, _id } = user
        const selectedCategory = categories.find(item => item.value == category)
        const avatar = photoUrl ? `${fileEndpoint}/${photoUrl}` : Images.defaultAvatar
        return (
            <SafeView bgColor={Color.primaryColor}>
                <Header
                    type={'back'}
                    title={'EDIT CONTACT'}
                />
                <Content width={'100%'}>
                    <View style={styles.container}>
                        <ProfileAvatar
                            source={contactUrl && contactUrl.uri || avatar}
                            size={Dimensions.px100}
                            onChangeAvatar={() => this.changeAvatar()}
                            style={{ marginTop: 20 }}
                        />
                        <EditInput
                            name={'Full Name'}
                            value={name}
                            width={'85%'}
                            onChangeText={name => updateContactDetail('user', { ...user, name })}
                        />
                        <EditInput
                            name={'Email'}
                            value={email}
                            width={'85%'}
                            onChangeText={email => updateContactDetail('user', { ...user, email })}
                        />
                        <EditInput
                            name={'Phone'}
                            value={phone}
                            width={'85%'}
                            onChangeText={phone => updateContactDetail('user', { ...user, phone })}
                        />
                        <View style={styles.flexContainer}>
                            <View style={{width:'85%', marginLeft:-10}}>
                                <EditInput
                                    name={'Linkedin Url'}
                                    value={linkedinUrl}
                                    width={'100%'}
                                    onChangeText={linkedinUrl => updateContactDetail('user', { ...user, linkedinUrl })}
                                />
                            </View>
                            <TouchableOpacity
                                onPress={() => NavigationService.navigate('Scanner', { goBack: Screens.EditContact })}
                            >
                                <Icon type={'MaterialCommunityIcons'} name={'qrcode-scan'} style={{ fontSize: Dimensions.px20, color: Color.primaryColor }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: '85%', marginBottom: 20 }}>
                            <Text style={{ fontSize: Dimensions.px14, marginBottom: 10, marginTop: 10, fontWeight: 'bold' }}>Category</Text>
                            <ModalDropdown
                                style={{ width: '100%', marginBottom: 20, borderBottomWidth: 1, borderBottomColor: Color.gray800 }}
                                textStyle={{ fontSize: Dimensions.px15, paddingBottom: 10 }}
                                defaultValue={selectedCategory && selectedCategory.label || 'Select'}
                                dropdownStyle={{ width: '85%' }}
                                dropdownTextStyle={{ fontSize: Dimensions.px14, color: Color.primaryColor }}
                                options={['Mentors', 'Investors', 'Other Founders', 'Developers']}
                                onSelect={(value) => updateContactDetail('user', { ...user, category: value + 1 })}
                            />
                        </View>
                        <Text style={styles.subTitle}>Key Strength</Text>
                        {
                            keyStrength.map(strenth => {
                                return <View style={styles.noteBox}>
                                    {SubDot}
                                    <Text style={styles.note}>{strenth}</Text>
                                </View>
                            })
                        }
                        <EditInput
                            name={''}
                            value={this.state.keyStrength}
                            width={'85%'}
                            onChangeText={keyStrength => this.setState({ keyStrength })}
                        />
                        <View style={styles.actionContainer}>
                            <AppButton
                                width={'30%'}
                                name={'Save'}
                                mgTop={5}
                                onPress={() => this.state.keyStrength && updateContactDetail('user', { ...user, keyStrength: keyStrength.concat(this.state.keyStrength) })}
                            />
                            <AppButton
                                bgColor
                                mgLeft={10}
                                width={'30%'}
                                name={'Cancel'}
                                mgTop={5}
                                onPress={() => this.setState({ keyStrength: '' })}
                            />
                        </View>
                        <Text style={styles.subTitle}>Note</Text>
                        {
                            note.map(txt => {
                                return <View style={styles.noteBox}>
                                    {SubDot}
                                    <Text style={styles.note}>{txt}</Text>
                                </View>
                            })
                        }
                        <EditInput
                            name={''}
                            value={this.state.note}
                            width={'85%'}
                            onChangeText={note => this.setState({ note })}
                        />
                        <View style={styles.actionContainer}>
                            <AppButton
                                width={'30%'}
                                name={'Save'}
                                mgTop={5}
                                onPress={() => this.state.note && updateContactDetail('user', { ...user, note: note.concat(this.state.note) })}
                            />
                            <AppButton
                                bgColor
                                mgLeft={10}
                                width={'30%'}
                                name={'Cancel'}
                                mgTop={5}
                                onPress={() => this.setState({ note: '' })}
                            />
                        </View>
                        <AppButton
                            width={'50%'}
                            name={'SAVE CONTACT'}
                            mgTop={20}
                            onPress={() => this.verifyEditContact()}
                        />

                    </View>
                </Content>
            </SafeView>
        )
    }
}
const mapStateToProps = ({ auth, contact }) => {
    return {
        AuthUser: auth,
        Contact: contact

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContactPage)
