import { Color, Dimensions } from '@constant/index'
export default {
    container: {
        width: Dimensions.pro100,
        height: Dimensions.pro100,
        alignItems: 'center',
        paddingBottom: 20
    },
    flexContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between',
        width: '85%',
        marginBottom:20
        
    },
    actionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-end',
        paddingRight: Dimensions.px30
    },
    subTitle: {
        marginTop: 10,
        width: '85%',
        fontSize: Dimensions.px16,
        fontWeight: 'bold',
        marginBottom: 10
    },
    noteBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '85%',
    },
    note: {
        width: '90%',
        paddingLeft: 10,
        marginBottom: 5
    }

}
