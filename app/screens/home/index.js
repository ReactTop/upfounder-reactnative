import { AppButton, SafeView, Header } from '@components/index'
import { DefaultStyles, Images, Screens, Color, Dimensions } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import { ActionCreators } from '@redux/action.js'
import React, { Component } from 'react'
import { ImageBackground, Image, Text, View, AsyncStorage, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from './styles'
import { Icon, Content } from 'native-base'
import SearchBar from './Header'
import { fileEndpoint } from '@api/Endpoint'
import * as Services from '@api/index'
const categories = [
  { category: 'Mentors', categoryId: 1, key: 'mentors' },
  { category: 'Investors', categoryId: 2, key: 'investors' },
  { category: 'Other Founders', categoryId: 3, key: 'founders' },
  { category: 'Developers', categoryId: 4, key: 'developers' },
]

class HomePage extends Component {
  state = {
    active: [],
    search: null,
    filterData: []
  }
  componentDidMount() {
    this.sub = this.props.navigation.addListener('didFocus', this.handleInit)
  }
  componentWillUnmount() {
    if (this.sub) this.sub.remove()
  }
  handleInit = () => {
    this.syncContacts()
    this.props.myProfile()
  }

  syncContacts() {
    categories.map(item => {
      console.log(item)
      this.props.getContactByCategory(item.categoryId, item.key)
    })

  }
  handleDataFilter(search) {
    const { mentors, investors, founders, developers } = this.props.AuthUser
    const originalData = [...mentors, ...investors, ...founders, ...developers]
    const filterData = originalData.filter(item => {
      const inputData = item.name && item.name.toUpperCase()
      const searchValue = search.toUpperCase()
      const outData = inputData.indexOf(searchValue) > -1
      return outData
    })
    this.setState({ filterData })
    console.log({ search, originalData, filterData })
  }

  render() {
    const { active, filterData, search } = this.state
    return (
      <SafeView bgColor={Color.primaryColor}>
        <View style={styles.container}>
          <SearchBar
            value={search}
            onChangeText={(search) => this.setState({ search })}
            onPress={() => this.handleDataFilter(search)}
            onScanner={() => {
              this.props.clearContact();
              NavigationService.navigate('Scanner', { goBack: Screens.Contact })
            }}
            onProfile={() => NavigationService.navigate(Screens.Profile)}
          />
          <Content width={'90%'}>
            {
              filterData.length ?
                <>
                  {
                    filterData.map(user => {
                      const { name, phone, photoUrl, email, linkedinUrl, _id, note, keyStrength } = user
                      const avatar = photoUrl ? `${fileEndpoint}/${photoUrl}` : Images.defaultAvatar
                      return (
                        <TouchableOpacity style={styles.flexContainer}
                          onPress={() => {
                            this.props.editContact(user, Screens.ContactDetail);
                          }}
                        >
                          <Image
                            source={{ uri: avatar }}
                            style={styles.avatar}
                          />
                          <View style={styles.userInformation}>
                            <Text style={styles.name}>{name}</Text>
                            <Text style={styles.note}>{email}</Text>
                          </View>
                        </TouchableOpacity>
                      )
                    })}
                </>
                :
                <>
                  <Text style={styles.title}>Contacts</Text>
                  {
                    categories.map(item => {
                      const data = this.props.AuthUser[item.key]
                      const isActive = active.includes(item.key)
                      return (
                        <View style={styles.subContainer}>
                          <TouchableOpacity style={styles.dropheader}
                            onPress={() => this.setState({ active: isActive ? active.filter(sub => sub != item.key) : active.concat(item.key) })}
                          >
                            <View style={styles.flex}>
                              <Icon
                                type={'FontAwesome'}
                                name={'user'}
                                style={{ fontSize: Dimensions.px20, marginRight: 10 }}
                              />
                              <Text style={styles.subTitle}>{item.category}</Text>
                            </View>
                            <Icon
                              type={'AntDesign'}
                              name={!isActive ? 'caretdown' : 'caretup'}
                              style={{ fontSize: Dimensions.px20 }}
                            />
                          </TouchableOpacity>
                          <View>
                            {
                              isActive && data.map(user => {
                                const { name, phone, photoUrl, email, linkedinUrl, _id, note, keyStrength } = user
                                const avatar = photoUrl ? `${fileEndpoint}/${photoUrl}` : Images.defaultAvatar
                                return (
                                  <TouchableOpacity style={styles.flexContainer}
                                    onPress={() => {
                                      this.props.editContact(user, Screens.ContactDetail);
                                    }}
                                  >
                                    <Image
                                      source={{ uri: avatar }}
                                      style={styles.avatar}
                                    />
                                    <View style={styles.userInformation}>
                                      <Text style={styles.name}>{name}</Text>
                                      <Text style={styles.note}>{email}</Text>
                                    </View>
                                  </TouchableOpacity>
                                )
                              })}
                          </View>
                        </View>
                      )
                    })
                  }
                </>
            }
          </Content>
        </View>
        <TouchableOpacity
          style={styles.addContact}
          onPress={() => {
            this.props.clearContact();
            NavigationService.navigate(Screens.Contact)
          }}
        >
          <Icon
            type={'AntDesign'}
            name={'plus'}
            style={{ fontSize: 30, color: Color.primaryColor }}
          />
        </TouchableOpacity>
      </SafeView>
    )
  }
}
const mapStateToProps = ({ auth }) => {
  return {
    AuthUser: auth,

  }
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage)
