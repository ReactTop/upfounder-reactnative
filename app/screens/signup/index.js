import { InputItem, SafeView } from '@components/index'
import { Color, Css, DefaultStyles, Dimensions, Images, Screens } from '@constant/index'
import NavigationService from '@navigation/NavigationService'
import { ActionCreators } from '@redux/action.js'
import { Content } from 'native-base'
import React, { Component } from 'react'
import { Image, Text, TouchableOpacity, View, Keyboard } from 'react-native'
import { UIActivityIndicator } from 'react-native-indicators'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from './styles'
const validemail = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email address' : undefined
class SignupPage extends Component {
  state = {
    name: null,  
    email: null,    
    password: null,
    repassword: null,
    activeInput: false,
    keyboardHeight: 0,
  }
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => { this.setState({ activeInput: true, keyboardHeight: e.endCoordinates.height }) });
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', (e) => { this.setState({ activeInput: false }) });
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  AppSignUp() {
    let { name, email, password, repassword } = this.state
    if (name === null) return alert('Please Enter Your Name')
    if (validemail(email) !== undefined) return alert(validemail(email.trim()))
    if (password !== repassword || password === null) return alert('Please Confrim Password')
    let profile = { name, email: email.trim(), password }    
    this.props.signUpUser(email.trim(), password, name)
  }
  render() {
    let {
      name,
      email,
      password,
      repassword,
      keyboardHeight,
      activeInput
    } = this.state

    let UserInfo = this.props.UserInfo
    return (
      <SafeView>
        <Content width={Dimensions.pro100}>
          <View style={[DefaultStyles.container]}>
            <Image
              source={Images.LogoUrl}
              style={[DefaultStyles.logo, { marginTop: Dimensions.px50, marginBottom: Dimensions.px20 }]}
            />
            <View style={DefaultStyles.alignCenter}>
              <InputItem
                placeholder='Name'
                IconName='user'
                IconType='Entypo'
                onChangeText={text => {
                  this.setState({ name: text })
                }}
                value={name}
              />

              <InputItem
                placeholder='Email Address'
                IconName='email'
                IconType='MaterialCommunityIcons'
                onChangeText={text => {
                  this.setState({ email: text })
                }}
                value={email}
              />

              <InputItem
                placeholder='Password'
                IconName='lock'
                IconType='FontAwesome5'
                onChangeText={text => {
                  this.setState({ password: text })
                }}
                value={password}
              />
              <InputItem
                placeholder='Confirm Password'
                IconName='lock'
                IconType='FontAwesome5'
                onChangeText={text => {
                  this.setState({ repassword: text })
                }}
                value={repassword}
              />
              <TouchableOpacity
                style={styles.authBtn}
                onPress={() => {
                  this.AppSignUp()
                }}
              >
                {UserInfo.loading !== true ? (
                  <Text style={styles.btnText}>Sign Up</Text>
                ) : (
                    <UIActivityIndicator color={'white'} size={Dimensions.px20} />
                  )}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => NavigationService.navigate(Screens.Login)}
                style={{
                  flexDirection: Css.flex.row,
                  justifyContent: Css.position.center,
                  alignItems: Css.position.center,
                  marginBottom: Dimensions.px50
                }}
              >
                <Text style={{ color: Color.black, fontSize: Dimensions.px14 }}>Do you have an account?</Text>
                <Text style={[styles.btnText, { color: Color.primaryColor, marginLeft: Dimensions.px10 }]}>Sign In</Text>
              </TouchableOpacity>
              <View height={activeInput ? keyboardHeight / 2 : 0} />
            </View>
          </View>
        </Content>
      </SafeView>
    )
  }
}
const mapStateToProps = ({ auth }) => {
  return {
    UserInfo: auth
  }
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupPage)
