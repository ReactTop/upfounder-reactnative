import { InputItem } from '@components/index'
import { Color, Css, DefaultStyles, Dimensions, Images, Screens } from '@constant/index'
import { ActionCreators } from '@redux/action.js'
import React, { Component } from 'react'
import { Animated, Image, Keyboard, ScrollView, Text, TouchableOpacity, View, SafeAreaView, KeyboardAvoidingView, Platform, AsyncStorage } from 'react-native'
import { SkypeIndicator, UIActivityIndicator } from 'react-native-indicators'
import { connect } from 'react-redux'
import { Icon } from 'native-base'
import { bindActionCreators } from 'redux'
import styles from './styles'
import NavigationService from '@navigation/NavigationService'
import LinkedInModal, { LinkedInToken } from 'react-native-linkedin'
import { safeGetOr } from '@utils/fp'
import { showAlert } from '@utils/index'
import * as Services from '@api/index'


const validemail = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email address' : undefined
class LoginPage extends Component {
  state = {
    linkedinSync: false,
    activeInput: false,
    keyboardHeight: 0,
    email: '',
    password: '',
    loading: true,
    loginModalVisible: false,
    registerModalVisible: false,
    offsetY: new Animated.Value(0),
    fadeAnim: new Animated.Value(0),
    indCatorVisible: 'flex'
  }
  modal = React.createRef()
  componentDidMount() {
    this.autoLogin()
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => { this.setState({ activeInput: true, keyboardHeight: e.endCoordinates.height }) });
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', (e) => { this.setState({ activeInput: false }) });
    this.intervalId = setInterval(() => {
      this.setTimePassed()
    }, 3000)
  }
  async autoLogin() {
    const loginCompleted = await AsyncStorage.getItem("loginCompleted")
    const email = await AsyncStorage.getItem("email")
    const password = await AsyncStorage.getItem("password")
    if (loginCompleted === "true" && email && password) this.props.loginUser({ email, password })
    console.log('Auto Login=???')
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  async setTimePassed() {
    this.setState({ loading: false })
    this.logoAnimation()
    clearInterval(this.intervalId)
  }

  logoAnimation() {
    Animated.timing(this.state.offsetY, {
      toValue: -150,
      duration: 500
    }).start()
    Animated.timing(this.state.fadeAnim, {
      toValue: 1,
      duration: 2000
    }).start()
  }
  AppLogIn() {
    let { email, password } = this.state
    if (validemail(email.trim()) !== undefined) return alert(validemail(email))
    if (password === '' || email === '') return alert('Please confrim email and password!')
    this.props.loginUser({ email: email.trim(), password })
  }
  getUser = async (data) => {
    const { access_token, authentication_code } = data
    if (!authentication_code) {
      const config = {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + access_token,
        }
      }
      this.setState({ linkedinSync: true })
      const responseBasicProfile = await fetch('https://api.linkedin.com/v2/me', config)
      const basicProfile = await responseBasicProfile.json()
      const responseElements = await fetch('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))', config)
      const elements = await responseElements.json()
      const password = safeGetOr(null, 'id')(basicProfile)
      const firstName = safeGetOr('', 'firstName.localized.en_US')(basicProfile)
      const lastName = safeGetOr('', 'lastName.localized.en_US')(basicProfile)
      const name = `${firstName} ${lastName}`
      const email = safeGetOr(null, 'elements[0].handle~.emailAddress')(elements)
      console.log('Sucess Get User', { password, name, email })
      Services.signUp(email, password, name)
        .then((profile) => {
          console.log('Linkedin Login-Non-Exist User' ,profile)
          this.setState({ linkedinSync: false })
          this.props.loginUser({ email, password })
        })
        .catch((errMsg) => {
          console.log('Linkedin Login-Exist User' ,errMsg)
          this.setState({ linkedinSync: false })
          if(errMsg=='Email address exists in database.'){
            this.props.loginUser({ email, password })
          }          
        })
    } else {
      showAlert('OK', 'Invalid Authentication Code', 'Please try again.')
      this.setState({ linkedinSync: false })
    }
  }
  render() {
    let { loading, email, password, fadeAnim, activeInput, keyboardHeight, linkedinSync } = this.state

    let UserInfo = this.props.UserInfo
    return (
      <KeyboardAvoidingView
        keyboardVerticalOffset={65} // adjust the value here if you need more padding
        style={{ flex: 1 }}
      // behavior={Platform.OS === "ios" ? "padding" : null}
      >
        <SafeAreaView>
          <ScrollView >
            <View style={[styles.container]}>
              <View style={{ height: Dimensions.deviceHeight - 24, width: Dimensions.pro100 }}>

                <Animated.View style={[{ transform: [{ translateY: this.state.offsetY }] }, styles.animationBox]}>
                  <Image source={Images.LogoUrl} style={[DefaultStyles.logo]} />
                </Animated.View>
                {loading !== true ? (
                  <Animated.View style={[styles.authBox, { opacity: fadeAnim }]}>
                    <InputItem
                      placeholder='Email Address'
                      IconName='email'
                      IconType='MaterialCommunityIcons'
                      onChangeText={text => {
                        this.setState({ email: text })
                      }}
                      value={email}
                    />
                    <InputItem
                      placeholder='Password'
                      IconName='lock'
                      IconType='FontAwesome5'
                      onChangeText={text => {
                        this.setState({ password: text })
                      }}
                      value={password}
                    />
                    <TouchableOpacity
                      style={styles.authBtn}
                      onPress={() => {
                        this.AppLogIn()
                      }}
                    >
                      {!UserInfo.loading ? (
                        <Text style={styles.btnText}>Sign In</Text>
                      ) : (
                          <UIActivityIndicator color={Color.white} size={Dimensions.px20} />
                        )}
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.authBtn}
                      onPress={() => {
                        this.modal.current.open()
                      }}
                    >
                      {!linkedinSync && <Icon
                        name={'linkedin-square'}
                        type={'AntDesign'}
                        style={{ color: Color.white, marginRight: 20 }}
                      />}
                      {!linkedinSync && <Text style={styles.btnText}>Login with Linkedin</Text>}
                      {linkedinSync && <UIActivityIndicator color={Color.white} size={Dimensions.px20} />}
                    </TouchableOpacity>
                    <Text style={[styles.btnText, { marginTop: 10, color: Color.primaryColor }]}
                      onPress={() => NavigationService.navigate(Screens.Signup)}
                    >Sign Up</Text>
                  </Animated.View>
                ) : (
                    <Animated.View style={[{ flex: Css.flex.flex4, backgroundColor: Color.black0 }]}>
                      <View style={{ height: Dimensions.px50 }}>
                        <SkypeIndicator color={Color.primaryColor} size={Dimensions.px20} />
                      </View>
                    </Animated.View>
                  )}
              </View>
            </View>
            <LinkedInModal
              ref={this.modal}
              linkText=''
              shouldGetAccessToken={true}
              clientSecret={'ogXYko5Jjvzs1NQT'}
              clientID="86e6jvm90alqwg"
              redirectUri="https://upfounders.herokuapp.com/admin/HomePage"
              // clientSecret={'t3x9QaQfFGrKx0AL'}
              // clientID="86bouz70al8qy1"
              // redirectUri="https://tododeusa.com.ar/"
              onSuccess={this.getUser}
            />
            {/* <View height={activeInput ? keyboardHeight / 2 : 0} /> */}
          </ScrollView>

        </SafeAreaView>
      </KeyboardAvoidingView>
    )
  }
}
const mapStateToProps = ({ auth }) => {
  return {
    UserInfo: auth
  }
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage)
