import { Color, Dimensions } from '@constant/index'
export default {
    cancelText: {
        color: Color.BACKGROUND_WHITE,
        fontSize: Dimensions.px16,  
        fontWeight:'bold',      
        right: 10,
        top:10,
        zIndex: 100,
        position: 'absolute',
        height: Dimensions.px30
    },
}
