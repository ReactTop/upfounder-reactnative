const OrderDetails = {
  newOrderType:'newOrderType',  
  costcoLocation: 'costcoLocation',
  deliveryProducts: 'deliveryProducts', 
  serviceType: 'serviceType', 
  floor: 'floor',
  receiptPictures: 'receiptPictures',
  signaturePicture: 'signaturePicture',
  deliveryDate:'deliveryDate',
  firstName:'firstName',
  lastName:'lastName',
  phoneNumber:'phoneNumber',
  email:'email',
  address:'address',
  approvalCode:'approvalCode'
}

export default OrderDetails
