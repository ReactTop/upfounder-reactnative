import Auth from './Auth'

import Profile from './Profile'

import Order from './Order'

import OrderDetails from './OrderDetails'

export const AuthTypes = Auth
export const ProfileTypes = Profile
export const OrderTypes = Order
export const OrderDetailsTypes = OrderDetails
