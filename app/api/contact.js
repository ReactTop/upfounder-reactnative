
import WService from '@helpers/WebService'
var wservice = new WService()


export const addContact = (data) => {
    return new Promise((resolve, reject) => {
        wservice.addContact(data)
            .then((response) => {
                console.log(response)
                if (response.statusCode === 200 && response.body) {
                    resolve(response.body)
                } else {
                    reject(new Error(response.body))
                }
            })
            .catch(reject)
    })
}


