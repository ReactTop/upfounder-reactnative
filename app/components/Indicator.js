import { Color, Dimensions } from '@constant/index'
import React, { Component } from 'react'
import { View } from 'react-native'
import { UIActivityIndicator } from 'react-native-indicators'

class Indicator extends Component {
    render() {
        let { isLoading, size } = this.props
        if (isLoading) {
            return <View style={{ height: Dimensions.px40 }}>
                <UIActivityIndicator color={Color.primaryColor} size={size || Dimensions.px20} />
            </View>
        } else {
            return (
                null
            )
        }
    }
}
export default Indicator