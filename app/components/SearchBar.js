import { Color, Dimensions } from '@constant/index'
import { Icon } from 'native-base'
import React, { Component } from 'react'
import { StyleSheet, TextInput, View, TouchableOpacity } from 'react-native'
import Indicator from './Indicator'
const styles = StyleSheet.create({
    container: {
        shadowColor: Color.black,
        shadowOffset: {
            width: 0,
            height: 5
        },
        shadowOpacity: 0.2,
        shadowRadius: 10,
        backgroundColor: Color.white,
        width: Dimensions.pro95,
        height: Dimensions.px40,
        borderRadius: Dimensions.px12,
        paddingLeft: Dimensions.px5,
        //paddingRight: Dimensions.px50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: Dimensions.px5
    },
    input: {
        color: Color.black,
        fontSize: Dimensions.px15,
        marginLeft:50,
        marginRight:50
    },
    search: {
        //width: 50,
        position: 'absolute',
        right: Dimensions.px13,
        zIndex: 100,
        justifyContent: 'center',
        alignItems: 'center',

    },
    close: {
        position: 'absolute',
        left: Dimensions.px13,
        zIndex: 100
    }
})

export default class SearchBar extends Component {
    render() {
        let { onChangeText, value, placeholder, onPress, loading, style } = this.props
        return (
            <View style={[
                styles.container, style ? style : {}
            ]}>

                <TextInput
                    style={styles.input}
                    autoCapitalize='none'
                    placeholder={placeholder}
                    onChangeText={text => {
                        onChangeText(text)
                    }}
                    value={value ? value.toString() : value}
                />
                {!(value === null || value === "") && <TouchableOpacity
                    onPress={() => {
                        onChangeText('');
                        !loading && onPress && onPress()
                    }}
                    style={styles.close}
                >
                    <Icon type={'AntDesign'} name={'close'} style={{ fontSize: Dimensions.px20 }} />
                </TouchableOpacity>
                }
                <View style={styles.search}>
                    {!loading &&
                        <Icon
                            type={'FontAwesome'}
                            name={'search'}
                            style={{ fontSize: Dimensions.px18, color: Color.black, }}
                            onPress={() => !loading && onPress && onPress()}
                        />
                    }
                    {loading &&
                        <Indicator isLoading={true} />}
                </View>
            </View>
        )
    }
}
