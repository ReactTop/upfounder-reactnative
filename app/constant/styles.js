import { StyleSheet, Platform } from 'react-native'
import Color from './colors'
import Dimensions from './dimensions'

export default DefaultStyles = StyleSheet.create({
  alignCenter: {
    alignItems: 'center',
    flex: 1,    
    justifyContent: 'center'
  },
  back: {
    color: Color.white,
    marginRight: Dimensions.px10
  },
  commonText: {
    fontSize: Dimensions.px14
  },
  container: {
    alignItems: 'center',
    height: Dimensions.pro100,
    resizeMode: 'contain',
    width: Dimensions.pro100,
    backgroundColor:Color.gray960
  },
  descriptionText: {
    color: Color.gray430,
    fontFamily: 'Roboto',
    fontSize: Dimensions.px14,
    margin: Dimensions.px10
  },
  detailCard: {
    backgroundColor: Color.white,
    borderColor: Color.gray900,
    borderRadius: 7,
    borderWidth: 1,
    elevation: 2,
    margin: 12,
    padding: 10,
    shadowColor: Color.black,
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.2,
    shadowRadius: 7
  },
  flexContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '95%'
  },
  headerstyle: {
    backgroundColor: Color.primaryColor,
    flexDirection: 'row',
    height: Platform.OS === 'ios' ? (Dimensions.deviceHeight > 810 || Dimensions.deviceWidth > 810 ? 105 : 80) : 60,
    paddingLeft: 15,
    paddingTop: Platform.OS === 'ios' ? (Dimensions.deviceHeight > 810 || Dimensions.deviceWidth > 810 ? 65 : 50) : 15
  },
  homeLogo: {
    height: 150,
    width: 150
  },
  logo: {
    height: Dimensions.px100,
    marginTop: 220,
    resizeMode: 'contain',
    width: Dimensions.px300
  },
  mainContainer: {
    backgroundColor: Color.white,
    flex: 1,
    padding: 5
  },
  sidebarLogo: {
    borderRadius: 10,
    height: 100,
    resizeMode: 'contain',
    width: 300,
    margin:20
  },
  title: {
    color: Color.white,
    fontSize: 24,
    fontWeight: 'bold'
  }
})
