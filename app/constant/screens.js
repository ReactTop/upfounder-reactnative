const Screens = {
  Home: 'Home',
  Login: 'Login',
  Signup: 'Signup',
  Scanner: 'Scanner',
  Profile: 'Profile',
  Contact: 'Contact',
  ContactDetail: 'ContactDetail',
  EditContact: 'EditContact',
  EditProfile: 'EditProfile',
  ForgotPassword: 'ForgotPassword'
}

export default Screens
