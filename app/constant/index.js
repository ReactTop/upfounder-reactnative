import Colors from './colors'

import _Images from './images'

import _Screens from './screens'

import Styles from './styles'

import _Dimensions from './dimensions'

import _Css from './css'

export const Color = Colors
export const Images = _Images
export const Screens = _Screens
export const DefaultStyles = Styles
export const Dimensions = _Dimensions
export const Css = _Css
