import React, { Component } from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer';
import ReduxThunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reducers from '@redux/reducer'
import NavigationService from '@navigation/NavigationService'
import Login from '@screens/login'
import Signup from '@screens/signup'
import Home from '@screens/home'
import Scanner from '@screens/scanner'
import Profile from '@screens/profile'
import Contact from '@screens/contact'
import ContactDetail from '@screens/contact-detail'
import EditContact from '@screens/edit-contact'
import EditProfile from '@screens/profile-edit'
import { Dimensions } from '@constant/index'
import Sidebar from '@screens/side-bar'
import ForgotPassword from '@screens/forgot-password'

const MainStack = createStackNavigator(
  {
    Login: { screen: Login },
    Signup: { screen: Signup },
    Home: { screen: Home },
    Scanner: { screen: Scanner },
    Profile: { screen: Profile },
    Contact: { screen: Contact },
    ContactDetail: { screen: ContactDetail },
    EditContact: { screen: EditContact },
    EditProfile: { screen: EditProfile },
    ForgotPassword: { screen: ForgotPassword }
  },
  {
    // initialRouteName: "Sigunature",
    defaultNavigationOptions: ({ navigation }) => {
      return {
        header: null
      }
    }
  }
)


const RootStack = createDrawerNavigator(

  {
    MainStack: {
      screen: MainStack,
      defaultNavigationOptions: {
        drawerLockMode: 'locked-open',
      }
    }
  },
  {
    drawerWidth: Dimensions.deviceWidth * 0.8,
    contentComponent: Sidebar
  }
);

export default class App extends Component {
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk))
    const Apps = createAppContainer(RootStack)

    return (
      <Provider store={store}>
        <Apps
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef)
          }}
        />
      </Provider>
    )
  }
}
console.disableYellowBox = true
