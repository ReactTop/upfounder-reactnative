import * as AuthActions from './auth/AuthActions'
import * as ContactActions from './contact/actions'

export const ActionCreators = Object.assign({}, AuthActions, ContactActions)
