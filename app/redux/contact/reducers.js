import { ActionTypes } from '../ActionTypes'
const defaultUser = {
    _id: "",
    email: "",
    name: "",
    aboutMe: "",
    aboutCompany: "",
    phone: "",
    keyStrength: [],
    contactUrl: {},
    category: '',
    note: [],
    __v: 0,
    linkedinUrl: ""
}
export const ContactReducer = (
    state = {
        user: defaultUser,
        serverUrl: "",
        userId: "",
        closeStrength: true,
        closeNote: true,
        updatePw: false,
        keyStrength: [""],
        notes: [""],
        pictures: []
    },
    action
) => {
    console.log(action.type)
    switch (action.type) {
        
        case ActionTypes.UPDATE_CONTACT_DETAIL:
            return {
                ...state,
                [action.payload.type]: action.payload.data
            }
        case ActionTypes.CLEAR_CONTACT:
            return {
                ...state,
                user: defaultUser
            }
        case ActionTypes.EDIT_CONTACT:            
            return {
                ...state,
                user: action.payload
            }
        default:
            return state
    }
}
