import { combineReducers } from 'redux'
import { AuthReducer } from './auth/AuthReducer'
import { ContactReducer } from './contact/reducers'
export default combineReducers({
  auth: AuthReducer,
  contact: ContactReducer
})
